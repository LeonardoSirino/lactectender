import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AddMaterial from './src/components/AddMaterial';
import { updateLocalTables } from './src/controllers/TenderData/TenderData';

import useCachedResources from './src/hooks/useCachedResources';
import useColorScheme from './src/hooks/useColorScheme';
import Routes from './src/navigation/Routes';
import EditItem from './src/screens/EditScreens/EditItem';
import EditItemSubItens from './src/screens/EditScreens/EditItemTabs/EditItemSubItens';
import EditSubItem from './src/screens/EditScreens/EditSubItem';

// TODO create component for buttons container, this structure is used several times
// TODO create addMaterial component

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  updateLocalTables()

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        {/* <Routes />
        < StatusBar /> */}

        <AddMaterial
          onSubmit={() => { }}
          setModalStatus={() => { }}
        />

      </ SafeAreaProvider>
    );
  }
}
