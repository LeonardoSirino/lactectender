import React from "react";
import useGlobalHook from "use-global-hook";

import * as actions from "./actions";
import { globalActions, globalState, itemEditType } from "./types";

const initialState: globalState = {
    navigation: null,
    activeTender: {
        id: 0,
        name: 'Orçamento',
        generalMaterials: [],
        itens: [],
        mobilization: [],
        prorateGeneralMaterials: true,
        prorateMobilization: true
    },
    tenders: [],
    demoCounter: 0,
    activeItem: {
        generalMaterialsApportionment: 0,
        id: 0,
        materials: [],
        mobilizationApportionment: 0,
        name: '',
        subItens: [],
    },
    activeSubItem: {
        id: 0,
        manHourEntries: [],
        name: '',
    },
    itemEditType: itemEditType.SubItens,
};


const useGlobal = useGlobalHook<globalState, globalActions>(
    React,
    initialState,
    actions
);

export default useGlobal;
