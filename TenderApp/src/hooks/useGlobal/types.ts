import { itemType, manHourType, subItemType, tenderType } from "../../controllers/TenderTypes";

export interface globalState {
    navigation: any,
    tenders: Array<tenderType>,
    activeTender: tenderType,
    activeItem: itemType,
    activeSubItem: subItemType,
    demoCounter: number,
    itemEditType: itemEditType
}

export interface globalActions {
    setNavigation: (navigation: any) => void,
    setActiveTender: (tender: tenderType) => void,
    incrementDemoCounter: () => void,
    setActiveTenderName: (name: string) => void,
    setActiveItem: (item: itemType) => void,
    setActiveItemName: (name: string) => void,
    setActiveSubItem: (subItem: subItemType) => void
    addManHourEntry: (manHourEntry: manHourType) => void,
    setActiveSubItemName: (name: string) => void,
    setItemEditType: (itemEditType: itemEditType) => void,
}

export enum itemEditType {
    SubItens,
    Materials
}