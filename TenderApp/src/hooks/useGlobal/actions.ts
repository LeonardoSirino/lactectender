import { Store } from 'use-global-hook'
import { itemType, manHourType, subItemType, tenderType } from '../../controllers/TenderTypes';
import { globalActions, globalState, itemEditType } from './types';

export const setNavigation = (store: Store<globalState, globalActions>, navigation: any) => {
    store.state.navigation = navigation
}

export const setActiveTender = (store: Store<globalState, globalActions>, tender: tenderType) => {
    store.setState({
        ...store.state,
        activeTender: tender,
        demoCounter: store.state.demoCounter + 1
    })
}

export const setActiveTenderName = (store: Store<globalState, globalActions>, name: string) => {
    store.setState({
        ...store.state,
        activeTender: { ...store.state.activeTender, name }
    })
}

export const incrementDemoCounter = (store: Store<globalState, globalActions>) => {
    console.log('Call of increment demo counter')
    store.setState({ ...store.state, demoCounter: store.state.demoCounter + 1 })
}

export const setActiveItem = (store: Store<globalState, globalActions>, item: itemType) => {
    store.setState({ ...store.state, activeItem: item })
}

export const setActiveItemName = (store: Store<globalState, globalActions>, name: string) => {
    store.setState({
        ...store.state,
        activeItem: { ...store.state.activeItem, name }
    })

    updateActiveEntities(store)
}

export const setActiveSubItemName = (store: Store<globalState, globalActions>, name: string) => {
    store.setState({
        ...store.state,
        activeSubItem: { ...store.state.activeSubItem, name }
    })

    updateActiveEntities(store)
}

export const setActiveSubItem = (store: Store<globalState, globalActions>, subItem: subItemType) => {
    store.setState({ ...store.state, activeSubItem: subItem })
}

export const addManHourEntry = (store: Store<globalState, globalActions>, manHourEntry: manHourType) => {
    let activeSubItem = store.state.activeSubItem
    activeSubItem.manHourEntries.push(manHourEntry)

    store.setState({
        ...store.state,
        activeSubItem,
    })

    updateActiveEntities(store)
}

export const setItemEditType = (store: Store<globalState, globalActions>, itemEditType: itemEditType) => {
    console.log('Call of setItemEditType')
    console.log(itemEditType)
    store.setState({
        ...store.state,
        itemEditType,
    })
}

// Auxiliary actions
const updateActiveEntities = (store: Store<globalState, globalActions>) => {
    let activeSubItem = store.state.activeSubItem

    // Replace or insert the active subItem in the active item
    let activeItem = store.state.activeItem
    let subItemIndex = activeItem.subItens.findIndex(subItem => subItem.id === activeSubItem.id)
    if (subItemIndex === -1) {
        activeItem.subItens.push(activeSubItem)
    } else {
        activeItem.subItens[subItemIndex] = activeSubItem
    }

    // Replace or insert the active item in the active Tender
    let activeTender = store.state.activeTender
    let itemIndex = activeTender.itens.findIndex(item => item.id === activeItem.id)
    if (itemIndex === -1) {
        activeTender.itens.push(activeItem)
    } else {
        activeTender.itens[itemIndex] = activeItem
    }

    store.setState({
        ...store.state,
        activeSubItem,
        activeItem,
        activeTender
    })
}

