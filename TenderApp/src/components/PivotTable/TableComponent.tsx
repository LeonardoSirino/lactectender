import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Row, Rows, Table, TableWrapper, Col } from 'react-native-table-component';
import Colors from '../../styles/Colors';
import NavigationButton from '../NavigationButton';

const headHeigth = 40
const rowsHeigth = 30

interface TableComponentPropsType {
    data: Array<Array<number | string>>,
    headers: Array<JSX.Element>,
    hasDeleteColumn: boolean,
    deleteRowCallback?: (index: number) => void,
}

const TableComponent = (props: TableComponentPropsType) => {
    const [activeRowIndex, setActiveRowIndex] = useState(-1)

    const createDeleteColumn = () => {
        if (!props.hasDeleteColumn) {
            return <View />
        }
        const deleteCallback = props.deleteRowCallback as Function
        const voidDeleteContainer = <View style={styles.voidDeleteContainer} />

        let heightArr = [headHeigth]

        const deleteButtons = props.data.map((row, index) => {
            heightArr.push(rowsHeigth)
            if (index === activeRowIndex) {
                return (
                    <View style={styles.deleteButtonContainer}>
                        <NavigationButton
                            iconName="md-trash"
                            backgroundColor="transparent"
                            foregroundColor={Colors.red}
                            onPress={event => deleteCallback(index)}
                            size={30}
                        />
                    </View>
                )
            }
            else {
                return voidDeleteContainer
            }
        })

        return (
            <Col
                data={[voidDeleteContainer, ...deleteButtons]}
                style={styles.deleteColumn}
                heightArr={heightArr}
            />
        )
    }

    const createRowsElements = () => {
        const rowsElements = props.data.map((row, index) => {
            const rowStyle = index === activeRowIndex ? styles.activeDataRow : styles.dataRow
            const rowContent = row.map(value => {
                return (
                    <Text
                        style={styles.text}
                        numberOfLines={1}>
                        {value}
                    </Text>
                )
            })

            return (
                <TouchableOpacity
                    onPress={() => {
                        console.log(`Press on row ${index}`)
                        setActiveRowIndex(index)
                    }}
                    style={rowStyle}>
                    <Row
                        data={rowContent}
                    />
                </TouchableOpacity>
            )
        })

        return rowsElements
    }


    return (
        <View style={styles.container}>
            <Table borderStyle={{ borderWidth: 0 }} style={styles.mainTable}>
                {createDeleteColumn()}
                <TableWrapper style={styles.tableWrapper}>
                    <Row
                        data={props.headers}
                        style={styles.head}
                        textStyle={styles.text} />

                    <Col
                        data={createRowsElements()}
                        textStyle={styles.text}
                    />
                </TableWrapper>
            </Table>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 30,
        width: '100%',
    },
    head: {
        height: headHeigth,
    },
    text: {
        margin: 6,
        textAlign: "center",
        alignContent: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    mainTable: {
        borderWidth: 0,
        flexDirection: 'row',
        width: '100%',
    },
    tableWrapper: {
        width: '100%',
        margin: 0
    },
    deleteColumn: {
    },
    deleteButtonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: Colors.green,
        left: -15
    },
    dataRow: {
        backgroundColor: 'transparent',
        height: rowsHeigth
    },
    activeDataRow: {
        backgroundColor: Colors.secondaryLightColor,
        height: rowsHeigth
    },
    voidDeleteContainer: {
        flex: 1
    }
})

export default TableComponent
