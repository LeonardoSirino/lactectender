import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { sortBy } from 'underscore';

import { Ionicons } from '@expo/vector-icons';

import Colors from '../../styles/Colors';
import TableComponent from './TableComponent';

interface PivotTablePropstype {
    data: Array<Array<number | string>>,
    headers: Array<string>,
    hasDeleteColumn: boolean
}

interface headerElementInfoType {
    index: number,
    title: string,
    sort: string,
    active: boolean
}

export class PivotTable {
    private data: Array<Array<number | string>>
    private headers: Array<headerElementInfoType>
    private forceUpdate: Function = () => { }
    hasDeleteColumn: boolean

    constructor(props: PivotTablePropstype) {
        this.hasDeleteColumn = props.hasDeleteColumn
        this.data = props.data
        this.headers = props.headers.map((title, index) => {
            const header: headerElementInfoType = {
                index,
                title,
                sort: '',
                active: false
            }

            return header
        })
    }

    setUpdaterHook = (forceUpdate: Function) => {
        this.forceUpdate = forceUpdate
    }

    addRow = (data: Array<number | string>) => {
        this.data.push(data)
        this.forceUpdate()
    }

    deleteRow = (index: number) => {
        console.log(`Remove row ${index}`)
        this.data.splice(index, 1)
        this.forceUpdate()
    }

    setData = (data: Array<Array<number | string>>, skipUpdate = false) => {
        this.data = data
        if (!skipUpdate) { this.forceUpdate() }
    }

    setHeaders = (headers: Array<string>, skipUpdate = false) => {
        this.headers = headers.map((title, index) => {
            const header: headerElementInfoType = {
                index,
                title,
                sort: '',
                active: false
            }

            return header
        })
        if (!skipUpdate) { this.forceUpdate() }
    }

    // TODO the header creation is a responsability of TableComponent
    private createHeaderElement = (headerInfo: headerElementInfoType, index: number) => {
        let icon = <View />
        if (headerInfo.sort == 'ascending') {
            icon = <Ionicons name="ios-arrow-round-up" size={24} color={Colors.secondaryLightColor} style={styles.sortIcon} />
        } else if (headerInfo.sort == 'descending') {
            icon = <Ionicons name="ios-arrow-round-down" size={24} color={Colors.secondaryLightColor} style={styles.sortIcon} />
        }


        return (
            <View style={styles.headerContainer}>
                <TouchableOpacity
                    style={styles.headerButton}
                    onPress={() => { this.handleHeaderClick(index) }}
                >
                    {icon}
                    <Text style={styles.headerTitle}>
                        {headerInfo.title}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    private createHeaders = () => {
        return (
            this.headers.map((value, index) => (this.createHeaderElement(value, index)))
        )
    }

    private handleHeaderClick = (index: number) => {
        let currentState = { ...this.headers[index] }

        this.headers = this.headers.map(item => ({ ...item, active: false, sort: '' }))

        currentState.sort = currentState.sort === 'ascending' ? 'descending' : 'ascending'
        currentState.active = true

        this.headers[index] = currentState

        let aux = sortBy(this.data, row => row[index])

        this.data = currentState.sort === 'ascending' ? aux.reverse() : aux

        this.forceUpdate()
    }

    TableView = () => {
        const headerComponents = this.createHeaders()
        return (
            <TableComponent
                headers={headerComponents}
                data={this.data}
                hasDeleteColumn={this.hasDeleteColumn}
                deleteRowCallback={index => this.deleteRow(index)}
            />
        )
    }
}


const styles = StyleSheet.create({
    headerButton: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'transparent'
    },
    headerTitle: {
        fontSize: 16,
        color: Colors.secondaryLightColor,
    },
    headerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primaryColor,
        marginHorizontal: -2,
        padding: 0
    },
    sortIcon: {
        marginRight: 10
    }
})