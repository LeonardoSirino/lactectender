import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import InputSpinner from 'react-native-input-spinner';

import { manHourType, materialEntryType, workerType } from '../controllers/TenderTypes';
import Colors from '../styles/Colors';
import NavigationButton from './NavigationButton';

interface AddMaterialPropsType {
    setModalStatus: (status: boolean) => void,
    onSubmit: (material: materialEntryType) => void,
}

const AddMaterial = (props: AddMaterialPropsType) => {
    let selectedWorker: workerType
    let quantity = 0

    return (
        <View style={styles.modalView}>
            <Text style={styles.modalTitle}>Adicionar novo material</Text>

            <View style={styles.entryContainer}>

                <View style={styles.singleEntryContainer}>

                    <Text style={styles.entriesTitle}>Quantidade</Text>

                    <InputSpinner
                        max={200}
                        min={1}
                        step={1}
                        value={1}
                        type='real'
                        onChange={(num) => {
                            quantity = num
                        }}

                        style={{
                            backgroundColor: 'transparent',
                        }}

                        inputStyle={{
                            width: 50,
                            maxWidth: 50,
                            color: Colors.primaryColor
                        }}

                        buttonStyle={{
                            backgroundColor: 'transparent',
                        }}

                        buttonPressStyle={{
                            backgroundColor: Colors.primaryColor,
                        }}

                        buttonTextColor={Colors.primaryColor}
                    />
                </View>

                <View style={styles.singleEntryContainer}>

                    <Text style={styles.entriesTitle}>Valor unitário</Text>

                    <TextInput
                        keyboardType='decimal-pad'
                        editable
                        style={styles.numericEntry}
                        placeholder='R$ 1,00'
                        onEndEditing={text => { console.log(text) }}
                    ></TextInput>

                </View>

            </View>

            <View style={styles.buttonsContainer}>

                <NavigationButton
                    iconName="ios-close"
                    backgroundColor={Colors.red}
                    onPress={() => {
                        props.setModalStatus(false)
                    }}
                />

                <NavigationButton
                    iconName="ios-checkmark"
                    backgroundColor={Colors.primaryColor}
                    onPress={() => {
                        // onSubmit()
                        props.setModalStatus(false)
                    }}
                />

            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    modalView: {
        margin: 20,
        height: 500,
        width: 500,
        alignSelf: 'center',
        backgroundColor: Colors.secondaryLightColor,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalTitle: {
        color: Colors.primaryColor,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 30
    },
    entryContainer: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'nowrap',
        margin: 0
    },
    buttonsContainer: {
        width: '90%',
        position: 'absolute',
        bottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    singleEntryContainer: {
        alignItems: 'center'
    },
    entriesTitle: {
        fontWeight: 'bold',
        color: Colors.primaryDarkColor
    },
    numericEntry: {
        fontSize: 16,
        height: 40,
        // backgroundColor: Colors.pink,
        textAlign: 'center',

    }
})

export default AddMaterial;