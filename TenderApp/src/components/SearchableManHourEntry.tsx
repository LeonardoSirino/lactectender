import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { getWorkersTable } from '../controllers/TenderData/TenderData';
import { workerType } from '../controllers/TenderTypes';
import Colors from '../styles/Colors';

interface SMHEPropsType {
    selectItemCallback: (item: workerType) => void,
}

const SearchableManHourEntry = (props: SMHEPropsType) => {
    const [workers, setWorkers] = useState([])

    useEffect(() => {
        // Only executes on first render of page
        getWorkersTable().then((workersTable: Array<workerType>) => { setWorkers(workersTable) })
    }, [])


    return (
        <View style={{ flex: 1 }}>
            <SearchableDropdown
                onItemSelect={(item: workerType) => {
                    console.log('Selected item in drop down menu')
                    console.log(item)
                    props.selectItemCallback(item)
                }}

                containerStyle={{
                    padding: 5,
                    maxHeight: 300,
                    height: 300,
                    // backgroundColor: Colors.red,
                    width: 220,
                    maxWidth: 220,
                }}

                textInputStyle={{
                    padding: 12,
                    borderWidth: 1,
                    borderColor: '#ccc',
                    backgroundColor: '#FAF7F6',
                }}
                itemStyle={{
                    //single dropdown item style
                    padding: 10,
                    marginTop: 2,
                    backgroundColor: '#FAF9F8',
                }}
                itemTextStyle={{
                    //single dropdown item's text style
                    color: '#222',
                }}
                itemsContainerStyle={{
                    //items container style you can pass maxHeight
                    //to restrict the items dropdown height
                    maxHeight: '200',
                }}
                items={workers}
                defaultIndex={2}
                placeholder="Colaborador"
                resetValue={false}
                underlineColorAndroid="transparent"

            />
        </View>
    )
}


export default SearchableManHourEntry