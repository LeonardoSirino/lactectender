import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { itemOperations } from '../controllers/Operations/itemOperations';
import { tenderOperations } from '../controllers/Operations/tenderOperations';
import { materialsOperations } from "../controllers/Operations/materialsOperations";

import { itemType, materialEntryType, mobilizationType, subItemType, tenderType } from '../controllers/TenderTypes';
import Colors from '../styles/Colors';
import { subItemOperations } from '../controllers/Operations/subItemOperations';
import useGlobal from '../hooks/useGlobal';
import { globalActions } from '../hooks/useGlobal/types';

export enum ItemValueBlockModes {
    itemType = "itemType",
    subItemType = "subItemType",
    materialEntryType = "materialEntryType",
    mobilizationType = "mobilizationType",
    overviewItens = "overviewItens",
    overviewMaterial = "overviewMaterial",
    overviewMobilization = "overviewMobilization"
}

interface ItemValueBlockPropsType {
    data: itemType
    | subItemType
    | materialEntryType
    | mobilizationType
    | tenderType,

    blockType: ItemValueBlockModes,
    navigation?: any,
    globalActions?: globalActions
}

const renderItemType = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as itemType

    const cost = itemOperations.totalManHourCost(blockData)
    const manHour = itemOperations.totalManHour(blockData)

    const extraContainerStyle = {
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity
            style={[styles.smallContainer, extraContainerStyle]}
            onPress={() => {
                props.globalActions.setActiveItem(blockData)
                props.navigation.navigate('EditItem')
            }} >

            <Text style={[styles.blockTitle, extraTitleStyle]}>
                {blockData.name}
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`R$ ${cost}\n${manHour} HH`}
            </Text>
        </TouchableOpacity>
    )
}

const renderSubItemType = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as subItemType

    const cost = subItemOperations.totalManHourCost(blockData)
    const manHour = subItemOperations.totalManHour(blockData)

    const extraContainerStyle = {
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity
            style={[styles.smallContainer, extraContainerStyle]}
            onPress={() => {
                props.globalActions.setActiveSubItem(blockData)
                props.navigation.navigate('EditSubItem')
            }}
        >

            <Text style={[styles.blockTitle, extraTitleStyle]}>
                {blockData.name}
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`R$ ${cost}\n${manHour} HH`}
            </Text>
        </TouchableOpacity>
    )
}

const renderMaterialEntryType = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as materialEntryType

    const cost = materialsOperations.totalCost(blockData)

    const extraContainerStyle = {
        backgroundColor: Colors.red,
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity style={[styles.smallContainer, extraContainerStyle]}>
            <Text style={[styles.blockTitle, extraTitleStyle]}>
                {blockData.name}
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`R$ ${cost}`}
            </Text>
        </TouchableOpacity>
    )
}

const renderOverviewItens = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as tenderType

    let HHcost = 0
    let materialCost = 0
    let totalHours = 0

    if (blockData !== undefined) {
        HHcost = tenderOperations.totalManHourCost(blockData)
        materialCost = tenderOperations.itensMaterialCost(blockData)
        totalHours = tenderOperations.totalManHour(blockData)
    }

    const extraContainerStyle = {
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity
            style={[styles.bigContainer, extraContainerStyle]}
            onPress={() => {
                props.globalActions.setActiveTender(blockData)
                props.navigation.navigate('ItensOverview')
            }}>

            <Text style={[styles.blockTitle, extraTitleStyle]}>
                Itens do orçamento
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`HH: R$ ${HHcost}\nMateriais: R$ ${materialCost}\n${totalHours} HH`}
            </Text>
        </TouchableOpacity>
    )
}

const renderOverviewMaterials = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as tenderType

    let totalCost = 0

    if (blockData !== undefined) {
        totalCost = tenderOperations.generalMaterialsCost(blockData)
    }

    const extraContainerStyle = {
        backgroundColor: Colors.red,
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity
            style={[styles.bigContainer, extraContainerStyle]}
            onPress={() => { props.navigation.navigate('MaterialsOverview') }}>

            <Text style={[styles.blockTitle, extraTitleStyle]}>
                Materiais gerais
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`R$ ${totalCost}`}
            </Text>
        </TouchableOpacity>
    )
}

const renderOverviewMobilization = (props: ItemValueBlockPropsType) => {
    let blockData = props.data as tenderType

    let totalCost = 0

    if (blockData !== undefined) {
        totalCost = tenderOperations.totalMobilizationCost(blockData)
    }

    const extraContainerStyle = {
        backgroundColor: Colors.secondaryColor,
    }

    const extraTitleStyle = {
    }

    const extraDescriptionStyle = {
    }

    return (
        <TouchableOpacity style={[styles.bigContainer, extraContainerStyle]}>
            <Text style={[styles.blockTitle, extraTitleStyle]}>
                Mobilização
            </Text>

            <Text style={[styles.description, extraDescriptionStyle]}>
                {`R$ ${totalCost}`}
            </Text>
        </TouchableOpacity>
    )
}

const ItemValueBlock = (props: ItemValueBlockPropsType) => {
    const [globalState, globalActions] = useGlobal()

    const navigation = globalState.navigation

    const itemProps = { ...props, navigation, globalActions }

    let renderFunction = props => <View />

    switch (props.blockType) {
        case ItemValueBlockModes.itemType:
            renderFunction = renderItemType
            break

        case ItemValueBlockModes.overviewItens:
            renderFunction = renderOverviewItens
            break

        case ItemValueBlockModes.overviewMaterial:
            renderFunction = renderOverviewMaterials
            break

        case ItemValueBlockModes.overviewMobilization:
            renderFunction = renderOverviewMobilization
            break

        case ItemValueBlockModes.materialEntryType:
            renderFunction = renderMaterialEntryType
            break

        case ItemValueBlockModes.subItemType:
            renderFunction = renderSubItemType

        default:
            break
    }

    return (
        renderFunction(itemProps)
    );
}

const styles = StyleSheet.create({
    smallContainer: {
        margin: 10,
        height: 80,
        borderRadius: 5,
        backgroundColor: Colors.primaryColor,
        width: "80%",
    },
    bigContainer: {
        margin: 10,
        height: 150,
        borderRadius: 5,
        backgroundColor: Colors.primaryColor,
        justifyContent: "center",
        alignItems: "center",
        width: "90%"
    },
    blockTitle: {
        color: Colors.primaryTextColor,
        fontSize: 30,
        position: "absolute",
        left: 10,
        top: 5,
    },
    description: {
        color: Colors.secondaryLightColor,
        fontSize: 16,
        position: "absolute",
        right: 10,
        bottom: 5,
        textAlign: "right"
    },
    buttonStyle: {
        backgroundColor: Colors.pink
    }
})

export default ItemValueBlock;