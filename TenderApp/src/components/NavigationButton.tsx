import React from 'react';
import { TouchableOpacity, StyleSheet, GestureResponderEvent } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import Colors from '../styles/Colors';
import useGlobal from '../hooks/useGlobal';

// import { Container } from './styles';

interface NavigationButtonPropsType {
    iconName: string,
    size?: number,
    foregroundColor?: string,
    backgroundColor?: string,
    onPress?: (event: GestureResponderEvent) => void,
    navigationRoute?: string
}

const defaultProps: NavigationButtonPropsType = {
    iconName: '',
    size: 50,
    foregroundColor: Colors.primaryTextColor,
    backgroundColor: Colors.primaryColor,
    onPress: () => { },
    navigationRoute: ''
}

const NavigationButton = (props: NavigationButtonPropsType) => {
    const [globalState, globalActions] = useGlobal()
    props = { ...defaultProps, ...props }

    let onPressFunction
    if (props.navigationRoute === '') {
        onPressFunction = props.onPress
    } else {
        onPressFunction = () => {
            props.onPress()
            globalState.navigation.navigate(props.navigationRoute)
        }
    }

    return (
        <TouchableOpacity
            onPress={onPressFunction}
            style={[
                styles.button,
                {
                    width: props.size,
                    height: props.size,
                    borderRadius: props.size / 2,
                    backgroundColor: props.backgroundColor
                }
            ]}>
            <Ionicons name={props.iconName} size={props.size * 0.7} color={props.foregroundColor} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    }
})

export default NavigationButton;