import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import InputSpinner from 'react-native-input-spinner';

import { manHourType, workerType } from '../controllers/TenderTypes';
import Colors from '../styles/Colors';
import NavigationButton from './NavigationButton';
import SearchableManHourEntry from './SearchableManHourEntry';

interface AddManHourPropsType {
    setModalStatus: (status: boolean) => void,
    addRow: (item: manHourType) => void
}

const AddManHourEntry = (props: AddManHourPropsType) => {
    let selectedWorker: workerType
    let hours = 0

    return (
        <View style={styles.modalView}>
            <Text style={styles.modalTitle}>Adicionar carga horária</Text>

            <View style={styles.entryContainer}>

                <SearchableManHourEntry
                    selectItemCallback={(item: workerType) => {
                        console.log('Selected worker')
                        console.log(item)

                        selectedWorker = item
                    }}

                />

                <InputSpinner
                    max={200}
                    min={0}
                    step={0.5}
                    value={0}
                    type='real'
                    onChange={(num) => {
                        hours = num
                    }}

                    style={{
                        backgroundColor: 'transparent',
                    }}

                    inputStyle={{
                        width: 50,
                        maxWidth: 50,
                        color: Colors.primaryColor
                    }}

                    buttonStyle={{
                        backgroundColor: 'transparent',
                    }}

                    buttonPressStyle={{
                        backgroundColor: Colors.primaryColor,
                    }}

                    buttonTextColor={Colors.primaryColor}
                />


            </View>

            <View style={styles.buttonsContainer}>

                <NavigationButton
                    iconName="ios-close"
                    backgroundColor={Colors.red}
                    onPress={() => {
                        props.setModalStatus(false)
                    }}
                />

                <NavigationButton
                    iconName="ios-checkmark"
                    backgroundColor={Colors.primaryColor}
                    onPress={() => {
                        if (selectedWorker !== undefined) {
                            const manHourEntry: manHourType = {
                                hours,
                                id: 0,
                                worker: selectedWorker
                            }

                            props.addRow(manHourEntry)
                        }

                        props.setModalStatus(false)
                    }}
                />

            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    modalView: {
        margin: 20,
        height: 500,
        width: 500,
        alignSelf: 'center',
        backgroundColor: Colors.secondaryLightColor,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modalTitle: {
        color: Colors.primaryColor,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 30
    },
    entryContainer: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'nowrap',
        margin: 0
    },
    buttonsContainer: {
        width: '90%',
        position: 'absolute',
        bottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
})

export default AddManHourEntry;