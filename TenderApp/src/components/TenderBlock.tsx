import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { tenderType } from '../controllers/TenderTypes';
import Colors from '../styles/Colors';

interface tenderBlockPropsType {
    data: tenderType
}

const TenderBlock = (props: tenderBlockPropsType) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Tender block</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        height: 100,
        borderRadius: 5,
        backgroundColor: Colors.primaryColor,
        justifyContent: "center",
        alignItems: "center",
        width: "90%"
    },
    text: {
        color: Colors.primaryTextColor,
        fontSize: 30
    }
})

export default TenderBlock;