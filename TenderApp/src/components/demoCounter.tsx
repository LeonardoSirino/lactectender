import React from 'react';
import { View, Text } from 'react-native';
import useGlobal from '../hooks/useGlobal';

const DemoCounter = () => {
    const [globalState, globalActions] = useGlobal()

    return (
        <View>
            <Text> {globalState.demoCounter}</Text>
        </View>
    )
}

export default DemoCounter;