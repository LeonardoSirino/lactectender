import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import Colors from '../styles/Colors';

interface screenTitlePropstype {
    title: string,
    editable?: boolean,
    onChangeText?: (text: string) => void
}

const defaultProps: screenTitlePropstype = {
    title: '',
    editable: false,
    onChangeText: (text: string) => { }
}

const ScreenTitle = (props: screenTitlePropstype) => {
    props = { ...defaultProps, ...props }

    if (props.editable) {
        const [text, setText] = useState(props.title)
        const [style, setStyle] = useState(styles.editableTitle)

        return (
            <View style={styles.container}>
                <TextInput
                    style={style}
                    value={text}
                    editable

                    onChangeText={text => {
                        setText(text)
                    }}

                    onFocus={() => {
                        setStyle(styles.editableTitleFocus)
                    }}

                    onBlur={() => {
                        props.onChangeText(text)
                        setStyle(styles.editableTitle)
                    }}
                />

            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <Text style={styles.title}> {props.title} </Text>
            </View>
        )
    }
}

export default ScreenTitle;

const styles = StyleSheet.create({
    title: {
        fontSize: 48,
        alignSelf: 'center',
        marginTop: 0,
        color: Colors.secondaryColor
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    editableTitle: {
        borderWidth: 0,
        width: '100%',
        fontSize: 48,
        color: Colors.secondaryColor,
        textAlign: "center",
    },
    editableTitleFocus: {
        borderWidth: 0,
        width: '100%',
        fontSize: 56,
        color: Colors.primaryColor,
        textAlign: "center",
    }
})