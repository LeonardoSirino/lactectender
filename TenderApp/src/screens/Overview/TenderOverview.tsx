import * as React from 'react';
import { StyleSheet } from 'react-native';

import { View, Text } from 'react-native';
import ItemValueBLock, { ItemValueBlockModes } from '../../components/ItemValueBlock';
import ScreenTitle from '../../components/ScreenTitle';
import useGlobal from '../../hooks/useGlobal';
import { sleep } from '../../util/timingFunctions';


export default function TenderOverview() {
  const [globalState, globalActions] = useGlobal()

  console.log('Log from Tender overview')
  console.log(globalState)

  return (
    <View style={styles.container}>
      <ScreenTitle
        title={globalState.activeTender.name}
        editable={true}
        onChangeText={globalActions.setActiveTenderName}
      />

      <View style={styles.blocksContainer}>
        <ItemValueBLock
          blockType={ItemValueBlockModes.overviewItens}
          data={globalState.activeTender}
        />

        <ItemValueBLock
          blockType={ItemValueBlockModes.overviewMaterial}
          data={globalState.activeTender}
        />

        <ItemValueBLock
          blockType={ItemValueBlockModes.overviewMobilization}
          data={globalState.activeTender}
        />
      </View>
    </View>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  blocksContainer: {
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
});
