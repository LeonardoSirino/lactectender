import * as React from 'react';
import { StyleSheet } from 'react-native';

import ItemValueBlock, { ItemValueBlockModes } from "../../components/ItemValueBlock";

import { Text, View } from 'react-native';
import { placeHolderData } from '../../data/placeHolderData';

export default function MaterialsOverview() {
    return (
        <View style={styles.container}>
            <ItemValueBlock
                blockType={ItemValueBlockModes.materialEntryType}
                data={placeHolderData.singleMaterial}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
