import * as React from 'react';
import { StyleSheet } from 'react-native';

import ItemValueBlock, { ItemValueBlockModes } from "../../components/ItemValueBlock";

import { Text, View, FlatList } from 'react-native';
import { itemType } from '../../controllers/TenderTypes';
import useGlobal from '../../hooks/useGlobal';
import NavigationButton from '../../components/NavigationButton';
import { getEmptyItem } from '../../controllers/TenderData/TenderData';
import Colors from '../../styles/Colors';

export default function ItensOverview() {
    const [globalState, globalActions] = useGlobal()

    const renderItem = ({ item }: { item: itemType }) => {
        return (
            <View style={styles.listItem}>
                <ItemValueBlock
                    blockType={ItemValueBlockModes.itemType}
                    data={item}
                />
            </View>
        )
    }

    const emptyList = () => {
        return (
            <View style={styles.emptyContainer}>
                <Text style={styles.emptyContent}>Sem itens</Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={globalState.activeTender.itens}
                renderItem={renderItem}
                ListEmptyComponent={emptyList}
            />

            < View style={styles.buttonsContainer} >
                <NavigationButton
                    iconName="ios-return-left"
                    backgroundColor={Colors.red}
                    navigationRoute='TenderOverview'
                />

                <NavigationButton
                    iconName="ios-add"
                    onPress={() => {
                        const newItem = getEmptyItem(globalState.activeTender)
                        globalActions.setActiveItem(newItem)
                        globalState.navigation.navigate('EditItem')
                    }}
                />
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    listItem: {
        alignItems: 'center',
        width: '100%'
    },
    emptyContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    emptyContent: {
        fontSize: 40
    },
    buttonsContainer: {
        width: '100%',
        paddingHorizontal: 20,
        position: 'absolute',
        bottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
