import * as React from 'react';
import { StyleSheet } from 'react-native';

import { Text, View } from 'react-native';

export default function EditMaterial() {

    return (
        <View style={styles.container}>
            <Text>Editar material</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
