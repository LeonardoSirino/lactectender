import * as React from 'react';
import { StyleSheet } from 'react-native';

import { Text, View } from 'react-native';

export default function EditMobilization() {

    return (
        <View style={styles.container}>
            <Text>Editar mobilização</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
