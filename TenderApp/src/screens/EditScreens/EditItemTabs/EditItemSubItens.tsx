import React, { useEffect } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import ItemValueBlock, { ItemValueBlockModes } from '../../../components/ItemValueBlock';
import { subItemType } from '../../../controllers/TenderTypes';
import Colors from '../../../styles/Colors';
import useGlobal from '../../../hooks/useGlobal';
import { itemEditType } from '../../../hooks/useGlobal/types';

const EditItemSubItens = ({ navigation }) => {
  const [globalState, globalActions] = useGlobal()
  const item = globalState.activeItem

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      globalActions.setItemEditType(itemEditType.SubItens)
    });

    return unsubscribe;
  }, [navigation]);

  const renderSubItem = ({ item }: { item: subItemType }) => {
    return (
      <View style={styles.listItem}>
        <ItemValueBlock
          data={item}
          blockType={ItemValueBlockModes.subItemType}
        />
      </View>
    )
  }

  const emptyList = () => {
    return (
      <View style={styles.emptyContainer}>
        <Text style={styles.emptyContent}>Sem etapas</Text>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={item.subItens}
        renderItem={renderSubItem}
        keyExtractor={(item: subItemType) => item.id.toString()}
        ListEmptyComponent={emptyList}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25,
    marginBottom: 70
  },
  listItem: {
    alignItems: 'center',
    width: '100%'
  },
  listTitle: {
    fontSize: 36,
    textAlign: "left",
    alignSelf: 'flex-start',
    marginLeft: 20,
    marginTop: 20
  },
  title: {
    fontSize: 48,
    alignSelf: 'center',
    marginTop: 20,
    color: Colors.secondaryColor
  },
  emptyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyContent: {
    fontSize: 30
  },
});


export default EditItemSubItens;