
import * as React from 'react';
import { Alert, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import AddManHourEntry from '../../components/AddManHourEntry';
import NavigationButton from '../../components/NavigationButton';
import { PivotTable } from '../../components/PivotTable';
import ScreenTitle from '../../components/ScreenTitle';
import { manHourOperations } from '../../controllers/Operations/manHourOperations';
import { manHourType } from '../../controllers/TenderTypes';
import useForceUpdate from '../../hooks/useForceUpdate';
import useGlobal from '../../hooks/useGlobal';
import Colors from '../../styles/Colors';

let Modal;

if (Platform.OS === 'android') {
    Modal = require('react-native').Modal
} else {
    Modal = require('modal-enhanced-react-native-web').default
}



const EditSubItem: React.FC = () => {
    const [globalState, globalActions] = useGlobal()

    const subItem = globalState.activeSubItem

    const [newItemVisible, setNewItemVisible] = React.useState(false)

    let data = subItem.manHourEntries.map(entry => [
        entry.worker.name,
        entry.worker.titulation.name,
        entry.hours,
        manHourOperations.totalManHourCost(entry)]
    )

    const [table, setTable] = React.useState(new PivotTable({ headers: [], data: [], hasDeleteColumn: true }))
    const forceUpdate = useForceUpdate()

    React.useEffect(() => {
        // Only executes on first render of page
        const SubItensTable = new PivotTable(
            {
                headers: ['Nome', 'Cargo', 'Horas', 'Valor'],
                data,
                hasDeleteColumn: true
            }
        )

        SubItensTable.setUpdaterHook(forceUpdate)
        setTable(SubItensTable)
    }, [])


    const renderModalContent = () => {
        return (
            <AddManHourEntry
                setModalStatus={setNewItemVisible}
                addRow={(item: manHourType) => {
                    console.log('received item')
                    console.log(item)
                    table.addRow([
                        item.worker.name,
                        item.worker.titulation.name,
                        item.hours,
                        manHourOperations.totalManHourCost(item)])

                    globalActions.addManHourEntry(item)
                }}
            />
        )
    }

    const renderModal = () => {
        if (Platform.OS === 'android') {
            console.log('android render modal')
            return (
                <Modal
                    visible={newItemVisible}
                    animationType="fade"
                    transparent={true}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }} >

                    {renderModalContent()}

                </Modal>
            )
        } else {
            console.log('WEB render modal')
            return (
                <Modal
                    isVisible={newItemVisible}
                    onBackdropPress={() => {
                        Alert.alert("Modal has been closed.")
                    }}>

                    {renderModalContent()}

                </Modal>
            )
        }
    }

    return (
        <View style={styles.container}>
            {renderModal()}

            <View style={styles.subContainer}>
                <ScreenTitle
                    title={subItem.name}
                    editable={true}
                    onChangeText={globalActions.setActiveSubItemName}
                />

                <table.TableView />

                <View style={styles.buttonsContainer}>
                    <NavigationButton
                        iconName="ios-return-left"
                        backgroundColor={Colors.red}
                        navigationRoute='EditItem'
                    />

                    <NavigationButton
                        iconName="ios-add"
                        backgroundColor={Colors.primaryColor}
                        onPress={() => {
                            setNewItemVisible(!newItemVisible)
                            console.log('set new item visible')
                            console.log(newItemVisible)
                        }}
                    />
                </View>
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        marginHorizontal: 0,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    subContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        margin: 0
    },
    title: {
        fontSize: 48,
        alignSelf: 'center',
        marginTop: 20,
        color: Colors.secondaryColor
    },
    button: {
        backgroundColor: Colors.primaryColor,
        width: 200,
        height: 50
    },
    buttonsContainer: {
        width: '100%',
        position: 'absolute',
        bottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
});

export default EditSubItem