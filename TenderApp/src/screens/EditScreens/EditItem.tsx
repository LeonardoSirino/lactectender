import * as React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Colors from '../../styles/Colors';
import EditItemMaterials from './EditItemTabs/EditItemMaterials';
import EditItemSubItens from './EditItemTabs/EditItemSubItens';
import ScreenTitle from '../../components/ScreenTitle';
import useGlobal from '../../hooks/useGlobal';
import NavigationButton from '../../components/NavigationButton';
import { getEmptySubItem } from '../../controllers/TenderData/TenderData';
import { itemEditType } from '../../hooks/useGlobal/types';
import AddMaterial from '../../components/AddMaterial';

const Tab = createMaterialTopTabNavigator();

let Modal;

if (Platform.OS === 'android') {
    Modal = require('react-native').Modal
} else {
    Modal = require('modal-enhanced-react-native-web').default
}

export default function EditItem() {
    const [globalState, globalActions] = useGlobal()
    const item = globalState.activeItem

    const [materialModalvisible, setMaterialModalvisible] = React.useState(false)

    const renderModalContent = () => {
        return (
            <AddMaterial
                setModalStatus={setMaterialModalvisible} />

        )
    }

    const renderModal = () => {
        if (Platform.OS === 'android') {
            console.log('android render modal')
            return (
                <Modal
                    visible={materialModalvisible}
                    animationType="fade"
                    transparent={true}>

                    { renderModalContent()}

                </Modal >
            )
        } else {
            console.log('WEB render modal')
            return (
                <Modal
                    isVisible={materialModalvisible}>

                    {renderModalContent()}

                </Modal>
            )
        }
    }

    return (
        <View style={styles.container}>
            <ScreenTitle
                title={item.name}
                editable={true}
                onChangeText={globalActions.setActiveItemName}
            />
            {renderModal()}
            <Tab.Navigator
                tabBarPosition='bottom'
                backBehavior='none'
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused }) => {
                        if (route.name === 'EditItemSubItens') {
                            let iconColor = focused ? Colors.primaryColor : Colors.secondaryLightColor
                            return <Ionicons name={'md-list'} size={25} color={iconColor} />
                        } else if (route.name === 'EditItemMaterials') {
                            let iconColor = focused ? Colors.red : Colors.secondaryLightColor
                            return <Ionicons name={'ios-cart'} size={25} color={iconColor} />
                        }
                    },

                    tabBarLabel: ({ focused }) => {
                        if (route.name === 'EditItemSubItens') {
                            let tabLabelStyle = focused ? styles.focusedTabLabel : styles.tabLabel
                            let tabLabelColor = focused ? Colors.primaryColor : Colors.secondaryLightColor
                            return (<Text style={{ ...tabLabelStyle, color: tabLabelColor }}>Etapas</Text>)
                        } else if (route.name === 'EditItemMaterials') {
                            let tabLabelStyle = focused ? styles.focusedTabLabel : styles.tabLabel
                            let tabLabelColor = focused ? Colors.red : Colors.secondaryLightColor
                            return (<Text style={{ ...tabLabelStyle, color: tabLabelColor }}>Materiais</Text>)
                        }
                    }
                })}
                tabBarOptions={{
                    showIcon: true,
                    showLabel: false,
                    indicatorStyle: { backgroundColor: Colors.secondaryColor }
                }}
            >
                <Tab.Screen name="EditItemSubItens" component={EditItemSubItens} />
                <Tab.Screen name="EditItemMaterials" component={EditItemMaterials} />
            </Tab.Navigator>

            < View style={styles.buttonsContainer} >
                <NavigationButton
                    iconName="ios-return-left"
                    backgroundColor={Colors.red}
                    navigationRoute='ItensOverview'
                />

                <NavigationButton
                    iconName="ios-add"
                    onPress={() => {
                        if (globalState.itemEditType === itemEditType.SubItens) {
                            const newSubItem = getEmptySubItem(globalState.activeItem)
                            globalActions.setActiveSubItem(newSubItem)
                            globalState.navigation.navigate('EditSubItem')
                        } else if (globalState.itemEditType === itemEditType.Materials) {
                            setMaterialModalvisible(true)
                        }
                    }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    focusedTabLabel: {
        fontSize: 12,
        fontWeight: 'bold',
    },
    tabLabel: {
        fontSize: 12,
        fontWeight: '100',
    },
    buttonsContainer: {
        width: '100%',
        paddingHorizontal: 20,
        position: 'absolute',
        bottom: 55,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
