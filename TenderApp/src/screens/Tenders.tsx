import * as React from 'react';
import { Button, FlatList, StyleSheet, Text, View } from 'react-native';

import DemoCounter from '../components/demoCounter';
import NavigationButton from '../components/NavigationButton';
import TenderBlock from '../components/TenderBlock';
import { getEmptyTender, getTenders } from '../controllers/TenderData/TenderData';
import { tenderType } from '../controllers/TenderTypes';
import useGlobal from '../hooks/useGlobal';

interface TenderPropsType {
  navigation: any
}

export default function Tenders(props: TenderPropsType) {
  const [globalState, globalActions] = useGlobal()

  globalActions.setNavigation(props.navigation)

  getTenders().then(tenders => {
    globalState.tenders = tenders
  })

  const renderTenderBlock = (tender: tenderType) => {
    return (
      <TenderBlock
        data={tender} />
    )
  }

  return (
    <View style={styles.container} >
      {/* <DemoCounter />
      <Button
        onPress={() => { globalActions.incrementDemoCounter() }}
      >
        <Text>TESTE</Text>
      </ Button> */}
      <FlatList
        data={globalState.tenders}
        renderItem={renderTenderBlock}
      />

      < View style={styles.buttonsContainer} >
        <NavigationButton
          iconName="ios-add"
          onPress={() => {
            getEmptyTender().then(tender => {
              globalActions.setActiveTender(tender)
              globalState.navigation.navigate('TenderOverview')
            })
          }}
        />
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  buttonsContainer: {
    position: 'absolute',
    bottom: 20
  }
});
