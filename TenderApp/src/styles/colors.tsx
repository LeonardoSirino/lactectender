export default {
    primaryColor: '#006380',
    primaryLightColor: '#4891af',
    primaryDarkColor: '#003953',
    secondaryColor: '#5a5a5e',
    secondaryLightColor: '#d7d7d8',
    secondaryDarkColor: '#313134',
    primaryTextColor: '#ffffff',
    secondaryTextColor: '#ffffff',
    red: '#801e00',
    lightRed: '#b64d2b',
    darkRed: '#500000',
    green: '#00805e',
    blue: '#002280',
    purple: '#1e0080',
    pink: '#800062'
};