import * as React from 'react';

import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import useColorScheme from '../hooks/useColorScheme';
// import EditMaterial from '../screens/EditMaterial';
// import EditMobilization from '../screens/EditMobilization';
import EditItem from '../screens/EditScreens/EditItem';
import ItensOverview from '../screens/Overview/ItensOverview';
import MaterialsOverview from '../screens/Overview/MaterialsOverview';
import TenderOverview from '../screens/Overview/TenderOverview';
import Tenders from '../screens/Tenders';
import EditSubItem from '../screens/EditScreens/EditSubItem';

const Stack = createStackNavigator();

export default function Routes() {
  const colorScheme = useColorScheme();

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Tenders">
        <Stack.Screen name="Tenders"
          component={Tenders}
          options={{
            headerShown: true,
            headerTitle: 'Orçamento'
          }} />

        <Stack.Screen name="TenderOverview"
          component={TenderOverview}
          options={{
            headerShown: false,
            headerTitle: 'Editar orçamento'
          }} />

        <Stack.Screen name="ItensOverview"
          component={ItensOverview}
          options={{
            headerShown: false,
          }} />

        <Stack.Screen name="MaterialsOverview"
          component={MaterialsOverview}
          options={{
            headerShown: false,
          }} />

        <Stack.Screen name="EditItem"
          component={EditItem}
          options={{
            headerShown: false,
          }} />

        <Stack.Screen name="EditSubItem"
          component={EditSubItem}
          options={{
            headerShown: false,
          }} />

      </Stack.Navigator>
    </NavigationContainer>
  )
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/




