import {
    itemType, manHourType, materialEntryType, subItemType, tenderType, titulationType, workerType
} from '../controllers/TenderTypes';

const pesquisador: titulationType = {
    id: 1,
    name: 'PQ',
    unitValue: 100,
}

const workerLeo: workerType = {
    name: "Leo",
    titulation: pesquisador,
}

const workerPaulo: workerType = {
    name: "Paulo",
    titulation: pesquisador,
}

const singleManHour: manHourType = {
    worker: workerLeo,
    hours: 10,
    id: 1,
}

const singleMaterial: materialEntryType = {
    id: 1,
    name: "material de teste",
    quantity: 1,
    unitValue: 100
}

const singleSubItem: subItemType = {
    id: 1,
    manHourEntries: [
        singleManHour,
        {...singleManHour, worker: workerPaulo, hours: 20}
    ],
    name: "Etapa 1"
}

const singleItem: itemType = {
    id: 1,
    name: 'Item 1',
    generalMaterialsApportionment: 0,
    mobilizationApportionment: 0,
    subItens: [
        singleSubItem,
        { ...singleSubItem, name: 'Etapa 2', id: 2},
        { ...singleSubItem, name: 'Etapa 3', id: 3},
        { ...singleSubItem, name: 'Etapa 4', id: 4},
        { ...singleSubItem, name: 'Etapa 5', id: 5},
        { ...singleSubItem, name: 'Etapa 6', id: 6}
    ],
    materials: [singleMaterial],
}

const tender: tenderType = {
    id: 1,
    name: "Orçamento de teste",
    generalMaterials: [{ ...singleMaterial, unitValue: 120 }],
    itens: [singleItem],
    mobilization: [],
    prorateGeneralMaterials: true,
    prorateMobilization: true
}

export const placeHolderData = {
    singleItem,
    tender,
    singleMaterial,
    singleSubItem
}