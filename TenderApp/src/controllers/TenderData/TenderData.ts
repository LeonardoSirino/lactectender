import axios from 'axios';
import { sortBy } from 'underscore';

import AsyncStorage from '@react-native-community/async-storage';

import { itemType, subItemType, tenderType, titulationType, workerType } from '../TenderTypes';

const api = axios.create({
    baseURL: 'https://script.google.com/macros/s/AKfycbxira4ExfkR6bs4-xhU7wCtLj-bg8DvEjxgR-6l66d6j4MgfZvH/exec',
    timeout: 5000,
    headers: {
        'Content-Type': 'text/plain;charset=utf-8',
    }
})

export const updateLocalTables = async () => {
    let { data: titulationsTable } = await api.get('', { params: { table: 'titulations' } })
    let { data: workersTableRaw } = await api.get('', { params: { table: 'workers' } })

    titulationsTable = <Array<titulationType>>titulationsTable

    let workersTable: Array<workerType> = workersTableRaw.map(item => {
        const titulation = titulationsTable.find(titulation => titulation.name === item.titulation)
        const worker: workerType = {
            id: item.id,
            name: item.name,
            titulation
        }
        return worker
    })

    await AsyncStorage.setItem('workersTable', JSON.stringify(workersTable))
    await AsyncStorage.setItem('titulationsTable', JSON.stringify(titulationsTable))
}

export const getWorkersTable = async (sorted = true) => {
    const payload = await AsyncStorage.getItem('workersTable')

    let workersTable: Array<workerType> = JSON.parse(payload)

    if (sorted) {
        workersTable = sortBy(workersTable, worker => worker.name)
    }

    return workersTable
}

export const getTenders = async () => {
    const payload = await AsyncStorage.getItem('tenders')

    let tenders: Array<tenderType> = JSON.parse(payload)

    return tenders
}

export const getEmptyTender = async () => {
    let tenders = await getTenders()

    let lastID = -1

    if (tenders === null) {
        lastID = -1
    } else {
        const ids = tenders.map(tender => tender.id)
        lastID = Math.max(...ids)
    }

    const newTender: tenderType = {
        id: lastID + 1,
        name: `Orçamento ${lastID + 2}`,
        generalMaterials: [],
        itens: [],
        mobilization: [],
        prorateGeneralMaterials: true,
        prorateMobilization: true
    }

    return newTender
}

export const getEmptyItem = (activeTender: tenderType) => {
    const ids = activeTender.itens.map(item => item.id)

    let newID: number

    if (ids.length === 0) {
        newID = 0
    } else {
        newID = Math.max(...ids) + 1
    }

    const newItem: itemType = {
        id: newID,
        name: `Item ${newID + 1}`,
        generalMaterialsApportionment: 0,
        materials: [],
        mobilizationApportionment: 0,
        subItens: []
    }

    return newItem
}

export const getEmptySubItem = (activeItem: itemType) => {
    const ids = activeItem.subItens.map(subItem => subItem.id)

    let newID: number

    if (ids.length === 0) {
        newID = 0
    } else {
        newID = Math.max(...ids) + 1
    }

    const newSubItem: subItemType = {
        id: newID,
        manHourEntries: [],
        name: `Etapa ${newID + 1}`
    }

    return newSubItem
}