import { materialEntryType } from "../TenderTypes"

export const materialsOperations = {
    totalCost: (item: materialEntryType) => {
        const cost = item.quantity * item.unitValue

        return cost
    },
}
