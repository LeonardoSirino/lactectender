import { tenderType } from "../TenderTypes";
import { itemOperations } from "./itemOperations";

export const tenderOperations = {
    totalManHourCost: (tender: tenderType) => {
        const totalHHcost = tender.itens.reduce((sum: number, item) => (sum += itemOperations.totalManHourCost(item)), 0)

        return totalHHcost
    },

    itensMaterialCost: (tender: tenderType) => {
        const materialsCost = tender.itens.reduce((sum: number, item) => (sum += itemOperations.totalMaterialsCost(item)), 0)

        return materialsCost
    },

    generalMaterialsCost: (tender: tenderType) => {
        const generalMaterialsCost = tender.generalMaterials.reduce((sum: number, item) => (sum += item.quantity * item.unitValue), 0)

        return generalMaterialsCost
    },

    totalMaterialsCost: (tender: tenderType) => {
        return tenderOperations.itensMaterialCost(tender) + tenderOperations.generalMaterialsCost(tender)
    },

    totalManHour: (tender: tenderType) => {
        const totalHHcost = tender.itens.reduce((sum: number, item) => (sum += itemOperations.totalManHour(item)), 0)

        return totalHHcost
    },

    totalMobilizationCost: (tender: tenderType) => {
        const value = tender.mobilization.reduce((sum: number, item) => (sum += item.multiplier * item.unitValue), 0)

        return value
    },
}