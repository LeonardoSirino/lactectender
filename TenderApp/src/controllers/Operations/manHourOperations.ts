import { manHourType } from "../TenderTypes";

export const manHourOperations = {
    totalManHourCost: (item: manHourType) => {
        let value = 0
        if (item.overrideUnitValue !== undefined) {
            let unitValue = item.overrideUnitValue as number
            value = unitValue * item.hours
        } else {
            value = item.worker.titulation.unitValue * item.hours
        }

        return value
    },
}