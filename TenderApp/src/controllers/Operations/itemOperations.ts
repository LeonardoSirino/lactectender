import { itemType } from "../TenderTypes";
import { subItemOperations } from "./subItemOperations";

export const itemOperations = {
    totalManHourCost: (item: itemType) => {
        const totalHHcost = item.subItens.reduce((sum: number, subItem) => (sum += subItemOperations.totalManHourCost(subItem)), 0)

        return totalHHcost
    },

    totalMaterialsCost: (item: itemType) => {
        const materialsCost = item.materials.reduce((sum: number, item) => (sum += item.quantity * item.unitValue), 0)

        return materialsCost
    },

    totalManHour: (item: itemType) => {
        const totalHHcost = item.subItens.reduce((sum: number, subItem) => (sum += subItemOperations.totalManHour(subItem)), 0)

        return totalHHcost
    },
}
