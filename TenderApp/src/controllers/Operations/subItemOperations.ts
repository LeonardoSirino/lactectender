import { subItemType } from "../TenderTypes";
import { manHourOperations } from "./manHourOperations";

export const subItemOperations = {
    totalManHourCost: (item: subItemType) => {
        const totalHHcost = item.manHourEntries.reduce((sum: number, item) => (sum += manHourOperations.totalManHourCost(item)), 0)

        return totalHHcost
    },

    totalManHour: (item: subItemType) => {
        const totalHH = item.manHourEntries.reduce((sum: number, item) => (sum += item.hours), 0)

        return totalHH
    },
}