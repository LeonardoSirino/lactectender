export interface titulationType {
    id: number,
    name: string,
    unitValue: number
}

export interface workerType {
    id: number,
    name: string,
    titulation: titulationType,
}

export interface manHourType {
    id: number,
    hours: number,
    worker: workerType,
    overrideUnitValue?: number
}

export interface materialEntryType {
    id: number,
    name: string,
    unitValue: number,
    quantity: number,
}

export interface subItemType {
    id: number,
    name: string,
    manHourEntries: Array<manHourType>,
}

export interface itemType {
    id: number,
    name: string,
    subItens: Array<subItemType>,
    materials: Array<materialEntryType>,
    mobilizationApportionment: number,
    generalMaterialsApportionment: number,
}

export interface mobilizationType {
    id: number,
    name: string,
    unitValue: number,
    multiplier: number,
}

export interface tenderType {
    id: number,
    name: string,
    itens: Array<itemType>
    mobilization: Array<mobilizationType>
    generalMaterials: Array<materialEntryType>,
    prorateMobilization: boolean,
    prorateGeneralMaterials: boolean,
}

