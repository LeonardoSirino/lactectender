function getTableHeaders(sheet: GoogleAppsScript.Spreadsheet.Sheet): Array<string> {
    let headers: Array<string>

    const numCols = sheet.getMaxColumns()

    headers = sheet.getRange(1, 1, 1, numCols).getValues()[0]
    headers.filter((value: string) => value != '')

    return headers
}


export function getTableObject(tableName: string) {
    const sheet = SpreadsheetApp.getActive().getSheetByName(tableName)

    let headers = getTableHeaders(sheet)
    let data = sheet.getDataRange().getValues()
    data.splice(0, 1)

    let table = data.map((row: Array<string | number>) => {
        let rowObject = {}
        row.forEach((value, index) => {
            rowObject[headers[index]] = value
        })

        return rowObject
    })

    return table
}
