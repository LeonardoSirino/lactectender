import { getTableObject } from "./GSTables";

function testGetTable() {
    const table = getTableObject('titulations')
    Logger.log(table)
}

const header = ['ID', 'name', 'unitValue']
const data = [
    [1, 'Leo', 100],
    [2, 'Paulo', 120],
    [3, 'Leo2', 150]
]

const localTableTest = () => {
    let aux = data.map((row: Array<string | number>) => {
        let rowObject = {}
        row.forEach((value, index) => {
            rowObject[header[index]] = value
        })

        return rowObject
    })

    console.log(aux)
}

localTableTest()