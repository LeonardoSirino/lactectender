import { getTableObject } from "./GSTables"

function doGet(e: GoogleAppsScript.Events.DoGet) {
    const tableName = e.parameter.table as string

    const table = getTableObject(tableName)

    return ContentService.createTextOutput(JSON.stringify(table)).setMimeType(ContentService.MimeType.JSON)
}
