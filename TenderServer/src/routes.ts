
import express from 'express'
import { Request, Response } from 'express';

const routes = express.Router()

const rootHandler = (_req: Request, res: Response) => {
  console.log('root request')
  return res.send('Still working');
}

routes.get('/', rootHandler)

export default routes